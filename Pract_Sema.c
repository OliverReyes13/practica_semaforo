#include <stdio.h>
#include <stdlib.h>

int actual;
void Estados_Actual(int estado);
void Sig_Estado(int carro, int estado2);

typedef enum{
    go_Norte,
    wait_Norte,
    go_Este,
    wait_Este
} estados;

typedef enum{
    No_Carro,
    Carro_Este,
    Carro_Norte,
    Ambos_Lados
} carros;

int main(){
    int es;
    int array[5]={No_Carro, Carro_Norte, Carro_Este, Ambos_Lados, Carro_Norte};
    estados edosig;
    edosig=2;
    actual=edosig;
    int cont;

    do{
        printf("\nUsted Esta En: ");
        Estados_Actual(actual);
        Sig_Estado(array[cont], actual);
        cont++;
    }while(cont<=5);
    return 0;
}

void Sig_Estado(int carro, int estado2){
    estados edosig;
    switch(estado2){
        //Casos para go_Norte
        case 0:
            switch(carro){
                case 0:
                    edosig = go_Norte;
                    break;
                case 1:
                    edosig = wait_Norte;
                    break;
                case 2:
                    edosig = go_Norte;
                    break;
                case 3:
                    edosig = wait_Norte;
                    break;
            }
            break;
        //Caso para wait_Norte
        case 1:
            edosig = go_Este;
            break;
        //casos para go_Este
        case 2:
            switch(carro){
                case 0:
                    edosig = go_Este;
                    break;
                case 1:
                    edosig = go_Este;
                    break;
                case 2:
                    edosig = wait_Este;
                    break;
                case 3:
                    edosig = wait_Este;
                    break;
            }
            break;
        //caso para wait_Norte
        case 3:
            edosig = go_Norte;
            break;
    }
    actual=edosig;
}

void Estados_Actual(int estado){
    switch(estado){
        case 0:
            printf("goNorte");
            break;
        case 1:
            printf("waitNorte");
            break;
        case 2:
            printf("goEste");
            break;
        case 3:
            printf("waitEste");
            break;
    }
}
